﻿using FerryContract.RealDto;

namespace FerryContract
{
    public class CreateReservationDto
    {
        public Booker Booker { get; set; }
        public Trip OutboundTrip { get; set; }
        public Trip ReturnTrip { get; set; }
    }
}