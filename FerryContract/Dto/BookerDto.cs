﻿namespace FerryContract.RealDto
{
    public class Booker
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
