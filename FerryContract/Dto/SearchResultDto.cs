﻿using System.Collections.Generic;

namespace FerryContract.RealDto
{
    public class SearchResultDto
    {
        public IList<Trip> Trips { get; set; }
    }
}
