﻿using FerryContract.RealDto;

namespace FerryContract
{
    public class Reservation
    {
        public string ReservationId { get; set; }
        public Booker Booker { get; set; }
        public Trip OutboundTrip { get; set; }
        public Trip ReturnTrip { get; set; }
        public decimal Price { get; set; }
        public Vehicle Vehicle { get; set; }
        public int NumberOfPassengers { get; set; }
    }
}