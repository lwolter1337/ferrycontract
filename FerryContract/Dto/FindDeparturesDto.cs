﻿using System;

namespace FerryContract.RealDto
{
    public class FindDeparturesDto
    {
        public Harbor Origin { get; set; }
        public Harbor Destination { get; set; }
        public Vehicle Vehicle { get; set; }
        public int NumberOfPassengers { get; set; }
        public DateTime DateOfDeparture { get; set; }
    }
}
