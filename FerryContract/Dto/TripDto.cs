﻿using System;

namespace FerryContract.RealDto
{
    public class Trip
    {
        public Harbor Origin { get; set; }
        public Harbor Destination { get; set; }
        public DateTime TimeOfDeparture { get; set; }
        public decimal Price { get; set; }
        public TimeSpan Duration { get; set; }
    }
}