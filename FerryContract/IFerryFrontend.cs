﻿using System.Collections.Generic;
using FerryContract.RealDto;

namespace FerryContract
{
    public interface IFerryFrontend
    {
        /// <summary>
        ///     Finds a list of departures for the given params
        /// </summary>
        /// <param name="search"></param>
        /// <returns>Returns list of departures</returns>
        IEnumerable<Trip> FindDepartures(FindDeparturesDto search);

        /// <summary>
        ///     Reservation is confirmed.
        ///     If returnTrip is null there is no return trip.
        /// </summary>
        /// <param name="reservation">A compilation of the booker and outbound and inbound trips.</param>
        /// <returns></returns>
        Reservation CreateReservation(CreateReservationDto reservation);

        /// <summary>
        ///     Finds bookers reservation, for updating or deleting
        /// </summary>
        /// <param name="reservationNumber"></param>
        /// <param name="name"></param>
        /// <returns>the reservation found else Null</returns>
        Reservation FindReservation(string reservationNumber, string name);

        /// <summary>
        ///     Updates the reservation
        /// </summary>
        /// <param name="updatedReservation"></param>
        /// <returns>returns updated reservation</returns>
        Reservation UpdateReservation(Reservation updatedReservation);

        /// <summary>
        ///     Deletes the reservation
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns>true if reservation is deleted</returns>
        bool DeleteReservation(Reservation reservation);
    }
}