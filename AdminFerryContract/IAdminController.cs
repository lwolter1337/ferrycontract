﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FerryContract.DTO;

namespace FerryContract
{
    interface IAdminController
    {

        /// <summary>Add a departure to the persistence layer.</summary>
        /// <param name="departure">The departure to create</param>
        /// <returns>The created departure</returns>
        /// <exception cref="FerryContract.DTO.FerryException">thrown when something goes wrong. Message descripes the incident.</exception>
        DepartureDTO AddDeparture(DepartureDTO departure);

        /// <summary>gets a valid  list of routes that a ferry config with a configuration can sail</summary>
        /// <param name="ferryconfigid">The id of the configuration to look up</param>
        /// <returns>collection of valid routes that the ferry can sail</returns>
        /// <exception cref="FerryContract.DTO.FerryException">thrown when something goes wrong. Message descripes the incident.</exception>
        IEnumerable<RouteDTO> GetRoutesByFerryConfig(int ferryconfigid);

        /// <summary>Test method used only to get a list of ferry configs that will ease the system into the Admin "tilføj afgang" UC</summary>
        /// <returns>This of valid routes that the ferry can sail</returns>
        /// <exception cref="FerryContract.DTO.FerryException">thrown when something goes wrong. Message descripes the incident.</exception>
        IEnumerable<SimpleFerryConfigDTO> GetFerryConfigs();
    }
}
