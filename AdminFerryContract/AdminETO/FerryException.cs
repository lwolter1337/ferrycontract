﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FerryContract.DTO
{
    /// <summary> A Ferry transfer object</summary>
    public class FerryException : Exception
    {
        FerryException(string message) : base(message) { }
    }
}
