﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FerryContract.DTO {
    public class SimpleFerryConfigDTO {
        /// <summary>
        /// Id uniquely identidfying the FerryConfig 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Output name for the UI
        /// </summary>
        public string Name { get; set; }
    }
}
