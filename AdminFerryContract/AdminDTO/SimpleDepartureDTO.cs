﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FerryContract.DTO {
    public class SimpleDepartureDTO {
        public int Id { get; set; }
        public string Name{ get; set; }
    }
}
