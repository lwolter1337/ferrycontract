﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FerryContract.DTO
{
    /// <summary> A Departure transfer object</summary>
    public class DepartureDTO
    {
        /// <summary>The date and time the departure is scheduled</summary>
        public DateTime DepartureTime { get; set; }

        /// <summary>Id of the route the departure is scheduled to sail on</summary>
        public int RouteId { get; set; }

        /// <summary>FerryConfigurationID that's scheduled to sail the departure</summary>
        public int FerryConfigId { get; set; }
    }
}
