﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FerryContract.DTO {
    public class RouteDTO {
        /// <summary>
        /// Unique id, which is auto-generated for the RouteDTO
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name for the RouteDTO
        /// </summary>
        public string Origin { get; set; }
        public string Depature { get; set; }
    }
}
