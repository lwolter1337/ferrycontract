﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FerryContract.DTO
{
    /// <summary> A Schedule transfer object</summary>
    public class RouteAndFerryDTO
    {
        /// <summary>
        /// List of all routes
        /// </summary>
        public IEnumerable<RouteDTO> Routes { get; set; }

        /// <summary>
        /// List of all ferry configurations
        /// </summary>
        public IEnumerable<SimpleFerryConfigDTO> FerryConfig { get; set; }

        /// <summary>List of departures occuring this month</summary>
        //public IEnumerable<DepartureDTO> Departures { get; set; }

    }
}
